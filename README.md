# wls-uniswap

## An Open Source Handbook by the Dex Crew

## Introduction
This guide is for members of blockchain communities whose chains have a native token and wish to permissionlessly access the tens of millions of dollars of daily volume and tens of millions of dollars of daily liquidity available on Ethereum’s Uniswap.  Over time, this guide may be expanded to cover other DEXes, or additional guides may be published.  

This guide specifically targets Uniswap v2, and provides concrete information on exactly how to set up Uniswap for Whaleshares, but you should be able to use it for integration with any chain of your choosing.  

The founders of Whaleshares preferred doing DEXes one by one-- programming their exchange listings instead of buying them.  

For a long time, this left Whaleshares with just one listing, that charged massive fees of 2000 WLS to move in and out of Rudex via the gateway. 

https://market.rudex.org/#/market/RUDEX.WLS_BTS

A while later, a hive-engine DEX listing programmed by a member of the DEX Crew reinvigorated trading, because WLS could move onto and off of hive-engine without crazy-high Rudex fees:

https://hive-engine.com/?p=market&t=WLS

The hive engine listing caused Rudex to lower their fees by >50%.  

#### Uniswap
Uniswap has literally thousands of times the liquidity that either hive-engine or Rudex/BitShares has.  

Ethereum Infrastructure

We Gonna need:

* ERC-20 Token: WLS
* ETH/WLS Liquidity Pair Contracts
* USDT/WLS Liquidity Pair Contracts


In the Uniswap Discord, I posted:

```
Hi Uniswappers, I'm currently attempting to list a native token from another chain on Uniswap v2.


1) Do it on an eth testnet (which one? This is only my second time doing any eth development) 2) Create an ERC-20 token contract, with no supply in the beginning 
3) Issue ERC-20 peg token only when users send the token to an account on the non-ethereum chain (this account will be used as a bridge/gateway) 
4) Send the newly-issued ERC-20 peg token to that user's Ethereum wallet 
5) Now, the supply of the peg token in the account on the other chain should exactly match the total issued supply of the ERC-20 peg token 
6) Users can move the ERC-20 peg token on Ethereum as they please, so it's now possible for them to create ERC20peg/ETH and ERC20peg/USDT liquidity pools using Uniswap 
7) Trading can now occur because the liquidity pools are in place and users may go to uniswap.exchange and enter the ERC 20 peg token ‘sID to see the USDT and ETH pairs for the erc20 peg edition 
8) When users wish to exchange ERC20 edition for “Layer 1” token (move from eth ot the other chain), they send their token somewhere, maybe even to a contract with a memo that contains their username (or maybe just to an account and then the gateway process burns it?, little bit unclear as to the best practices here) ERC20 peg token edition is burned, and the tokens are sent to the user’s username/address on the other chain.  

Is this fundamentally correct?
```

No one was very interested in helping, but since Ethereum is fully permissionless, we were able to integrate with Uniswap anyway!


Gas Costs

Create the erc20 contract (one time)
Issue new eth.wls (every time funds come to @ethlink)
Burn eth.wls (every time funds leave ethereum for whaleshares)


Who is who on ethereum?
Ethereum doesn't have a memo field, but it does have transaction data.  

Why not use bip39 to make a seed (given to user exactly once for backup purposes) from which we generate an eth address that is stored by the wls user in a customjson transaction?

This way we know which wls user is who on eth. 

## ERC 20 Contract Creation
So to do this, we needed a contract.  At first I did not deploy an ideal contract.  

### The First, Failed WLS-ERC20
At first, I used a web-based erc20 generator and metamask:

https://vittominacori.github.io/erc20-generator/

To create this token: 

https://etherscan.io/tx/0xc743f0e9922c2a56c242056438eee0f94754541fc722f17e8e917b8f59c6d332#eventlog

But there were issues:

Ethereum defaults to 18 decimal places and WLS only has 3
Generator + metamask was expensive
Metamask can’t really be automated, this way of doing things had no ability to work well in the future

Therefore, you should consider this token:

https://etherscan.io/address/0xbe4a5d608ba33f55bc03b27023c417a9dc4898c4

As deprecated.  It will never be the WLS-token.  

## WLS-ERC20 the second

For the second whaleshares ERC20 token, I used [Web3](https://github.com/gochain/web3).

Web3 is a great tool that includes OpenZeppelin smart Contract Generation capabilities.  This means that your contracts will use audited code and meet the standards of one of Ethereum's most respected auditing bodies.  

#### Exact Steps Using Web3

**generate**
Generate a standard OpenZeppelin ERC20 contract with the symbol WLS, the name whaleshares, and three decimal points.

```bash
web3 generate contract erc20 --symbol WLS --name whaleshares -d 3
```

**build**
Compile your ERC20 contract.

```
web3 contract build WLS.sol
```

This will give you:
* WLS_flatten.sol
* WLS.bin
* WLS.abi


**deploy**
This deploys your contract to the Ethereum blockchain
```bash
web3 contract deploy WLS.bin
```

The New WLS ERC20 token lives at:
https://etherscan.io/address/0x7cff1ea4522906db354ccfe76da9ab748f76b29c

Here’s how to learn what you can do with your token:

```
web3 contract list --abi WLS.abi
function approve(address spender, uint256 value) returns(bool)
function decimals() constant returns(uint8)
function isPauser(address account) constant returns(bool)
function isMinter(address account) constant returns(bool)
function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
function transfer(address to, uint256 value) returns(bool)
function paused() constant returns(bool)
function renouncePauser() returns()
function balanceOf(address account) constant returns(uint256)
function addPauser(address account) returns()
function symbol() constant returns(string)
function addMinter(address account) returns()
function allowance(address owner, address spender) constant returns(uint256)
function name() constant returns(string)
function transferFrom(address from, address to, uint256 value) returns(bool)
function burnFrom(address account, uint256 amount) returns()
function renounceMinter() returns()
function totalSupply() constant returns(uint256)
function increaseAllowance(address spender, uint256 addedValue) returns(bool)
function unpause() returns()
function mint(address account, uint256 amount) returns(bool)
function burn(uint256 amount) returns()
function pause() returns()
```

Check token supply:
```
web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function totalSupply
0
```

The supply correctly returns zero

Now we are going to issue the exact number of WLS that exist in the ethlink account: 

* ethlink contains: 18,411.661 WLS 
* Chucky has been given : 200 ERC20-WLS 
* Issuing to myself for the liquidity pool: 18,211.661 ERC20-WLS
* translation of 18,211.661 WLS into eth-speak: 18211661


```
web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function mint "0x5e4868E860A6E12cd4deCb5B262E15208A9479F2" "18211661"
```


### Uniswap Integration
The uniswap integration probably the easiest part of this tutorial.  All I needed was some WLS and some ETH in my metamask wallet.  Then I went over to [Uniswap](https://uniswap.exchange), clicked "add pool", entered the token contract address, and paid about [$20 in fees](https://etherscan.io/address/0x7a250d5630b4cf539739df2c5dacb4c659f2488d) to create an eth/wls trading pair.  Since the web UI for this is so simple, effective and safe, this is the method that I'd advise anyone to use.  

Result: Whaleshares on Uniswap
https://uniswap.info/token/0x7cff1ea4522906db354ccfe76da9ab748f76b29c


# Automation
To automate this, we are going to use two JavaScript Libraries in a node.js application:

* Web3.js or [ethers.js](https://docs.ethers.io/v5/api/contract/example/)
* wlsjs

We will use web3.js to monitor for ethereum transactions coming out, which will have [data](https://medium.com/swlh/understanding-data-payloads-in-ethereum-transactions-354dbe995371) appended to them, specifically the whaleshares username that the WLS is destined to.  

In figuring out the automation, we also look at the manual steps.  By providing a user interface for the gateway, we can make it possible for the user to pay the burn fee when leaving Ethereum, and for the user to send a data-only transaction to the gateway address, limiting the gateway's responsiblity for Ethereum-side fees.  

### Coming into Ethereum

https://explorer.whaleshares.io/#/account/ethlink

Bank sends 10,000 WLS to ethlink with 0x7b1d3d3b42b28f1b1497d3c0d8a3b27d825e77c4 as memo

Use web3 to call the ERC20 contract's mint function:

```bash
web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function mint "0x7b1d3d3b42b28f1b1497d3c0d8a3b27d825e77c4" "10000000"

Transaction hash: 0x4fdba9fb539e936d0d78cdb8da1bfe647bc22c7e67db454b258d01b0c156e9b4
```

**note**
in eth-speak, we do not enter a decimal place.  10000000 is eth-speak for 10,000 with three decimal places, and the default is 18 decimal places, so watch out for that when you are creating your contract.  


### Ethereum to Whaleshares
Got 52.621 erc20-wls on Ethereum from thecryptodrive

Burned it

```bash
web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function burn "52621"

Transaction hash: 0x438f137cf34a1aaecd738eccf9d8668dec952829c78e349c07c45424f4dee9ba
```

Got erc20-wls on ethereum from crypto-hippy
```bash
❯ web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function balanceOf "0xbcDdE9Ec78f5b8b47c70106A7eBBAEC64a5CBE8A"

2119000
❯ web3 contract call --abi wls.abi --address 0x7cff1ea4522906db354ccfe76da9ab748f76b29c --function burn "2119000"

Transaction hash: 0x1e39559bdad5d207ea1e4183b5ad66d267a24a1d5c63ea4741921353f4981912
```


Sent it to thecryptodrive on whaleshares:
https://explorer.whaleshares.io/#/account/ethlink

So, that's the manual way.  
